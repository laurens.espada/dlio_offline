## Description

`dlio_offline` is an offline version of DLIO (Direct Lidar-Inertial Odometry)
for high resolution mapping. 

![Image of generated pointcloud of B747-400F](doc/747400_pointcloud.png)

## Dependencies

This package doesn't depend on anything that isn't already included in the ISAAC
dependencies, so you can proceed to installation. For completeness, I list the
dependencies here:
- `Eigen >= 3.3.7`
- `Point Cloud Library >= 1.10.0` and `pcl_ros`
- A compiler with OpenMP support (you probably already have this, GCC has it)

## Installation

To install, simply clone into your `~/mainblades_ws/src` directory and build:

```
cd ~/mainblades_ws/src
git clone https://gitlab.com/laurens.espada/dlio_offline.git
nav_build -l -w ~/mainblades_ws -p dlio_offline --no-warnings -nd
```

## Usage

`dlio_offline` works on bag files with `sensor_msgs/PointCloud2` and
`sensor_msgs/Imu` messages. If the bag contains Ouster lidar packets, make sure
to run `reconstruct_pointcloud` on the bag first.

To run `dlio_offline` on a single bag file, you can use the launchfile:
```
roslaunch dlio_offline dlio.launch rviz:=false bag_file:=/path/to/bag/file.bag
```
If everything is working, you will see the following diagnostics:
```
+-----------------------+
|PROGRESS:       43.38% |
|TIME ELAPSED:   8m11s  |
|TIME REMAINING: 10m41s |
+-----------------------+
```
When the process is finished, it will automatically save the pointcloud map to
`/path/to/bag/file.bag_map.pcd`.

To run dlio_offline for several bagfiles, you can use the included bash script:
```
./dlio_offline_batcher.sh file1.bag file2.bag ... fileN.bag
```

## Output
The final `.pcd` files generated by `dlio_offline` contain extra information in
each point. The `x_sensor`, `y_sensor`, `z_sensor` fields denote the position of
the LiDAR sensor in the global reference frame when that point was captured.

## Additional notes
For convenience, `dlio_offline` does not read from a specific topic for 
pointclouds and IMU messages, rather it reads every `sensor_msgs/PointCloud2` 
and `sensor_msgs/Imu` message in the rosbag sequentially. This is why there is
no way to set the pointcloud or IMU ROS topics. This is done to to speed up the
mapping process.

In August 2024, `dlio_offline` was used to re-create most of the existing 
pointclouds previously generated by Google Cartographer, because `dlio_offline` 
produces pointclouds with less noise and fewer artefacts to clean up.