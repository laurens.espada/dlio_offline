#!/bin/bash

if [ "$#" -lt 1 ]; then
  echo "Usage: $0 <bag_file1> <bag_file2> ... <bag_fileN>"
  exit 1
fi

# Function to filter expected but unwanted lines. Don't filter unexpected errors.
filter_output() {
  grep -v -E "logging to|Checking log directory|Press Ctrl-C to interrupt|Done checking log file disk usage|started roslaunch server|auto-starting new master|process\[master\]: started with pid|ROS_MASTER_URI|setting /run_id to|process\[rosout-1\]: started with pid|started core service \[/rosout\]|process\[.*\]: started with pid|killing on exit|shutting down processing monitor|============================|process has finished cleanly|log file:|Initiating shutdown|^$"
}

{
  for bag_file in "$@"
  do
    bag_file_fullpath=$(realpath $bag_file)
    if [ ! -f "$bag_file_fullpath" ]; then
      echo "Bag file $bag_file_fullpath does not exist"
      continue
    fi
    echo "Processing $bag_file_fullpath"
    roslaunch dlio_offline dlio.launch rviz:=false bag_file:=$bag_file_fullpath --no-summary || exit 1
    wait
    echo "Finished processing $bag_file_fullpath"
  done
} 2>&1 | filter_output