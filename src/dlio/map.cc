/***********************************************************
 *                                                         *
 * Copyright (c)                                           *
 *                                                         *
 * The Verifiable & Control-Theoretic Robotics (VECTR) Lab *
 * University of California, Los Angeles                   *
 *                                                         *
 * Authors: Kenny J. Chen, Ryan Nemiroff, Brett T. Lopez   *
 * Contact: {kennyjchen, ryguyn, btlopez}@ucla.edu         *
 *                                                         *
 ***********************************************************/

#include "dlio/map.h"

#include <filesystem>

dlio::MapNode::MapNode(ros::NodeHandle node_handle) : nh(node_handle) {

  this->getParams();

  this->keyframe_sub = this->nh.subscribe("keyframes", 10,
      &dlio::MapNode::callbackKeyframe, this, ros::TransportHints().tcpNoDelay());
  this->map_pub = this->nh.advertise<sensor_msgs::PointCloud2>("map", 100, true);
  this->save_pcd_srv = this->nh.advertiseService("save_pcd", &dlio::MapNode::savePcd, this);
  this->terminate_srv = this->nh.advertiseService("terminate_map_node", &dlio::MapNode::terminateMapNode, this);

  this->dlio_map = pcl::PointCloud<PointType>::Ptr (std::make_shared<pcl::PointCloud<PointType>>());

  pcl::console::setVerbosityLevel(pcl::console::L_ERROR);

}

dlio::MapNode::~MapNode() {}

void dlio::MapNode::getParams() {

  ros::param::param<std::string>("~dlio/odom/odom_frame", this->odom_frame, "odom");

  this->nh.getParam("map_filename", this->map_filename);

  // Get Node NS and Remove Leading Character
  std::string ns = ros::this_node::getNamespace();
  ns.erase(0,1);
}

void dlio::MapNode::start() {
}

void dlio::MapNode::callbackKeyframe(const sensor_msgs::PointCloud2ConstPtr& keyframe) {

  // convert scan to pcl format
  pcl::PointCloud<PointType>::Ptr keyframe_pcl =
    pcl::PointCloud<PointType>::Ptr (std::make_shared<pcl::PointCloud<PointType>>());
  pcl::fromROSMsg(*keyframe, *keyframe_pcl);

  // save filtered keyframe to map for rviz
  *this->dlio_map += *keyframe_pcl;

  // publish full map
  if (this->dlio_map->points.size() == this->dlio_map->width * this->dlio_map->height) {
    sensor_msgs::PointCloud2 map_ros;
    pcl::toROSMsg(*this->dlio_map, map_ros);
    map_ros.header.stamp = ros::Time::now();
    map_ros.header.frame_id = this->odom_frame;
    this->map_pub.publish(map_ros);
  }

}

bool dlio::MapNode::savePcd(dlio_offline::save_pcd::Request& req,
                            dlio_offline::save_pcd::Response& res) {

  pcl::PointCloud<PointType>::Ptr m =
    pcl::PointCloud<PointType>::Ptr (std::make_shared<pcl::PointCloud<PointType>>(*this->dlio_map));
  
  if (m->width * m->height != m->size()) {
    m->width = m->size();
    m->height = 1;
  }

  std::cout << "Saving map to " << this->map_filename << std::endl;

  // save map
  int ret = pcl::io::savePCDFileBinary(this->map_filename, *m);
  res.success = ret == 0;

  if (!res.success) {
    ROS_ERROR("SAVING MAP FAILED.");
  }

  return res.success;

}

bool dlio::MapNode::terminateMapNode(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res) {
    ros::shutdown();
    return true;
}