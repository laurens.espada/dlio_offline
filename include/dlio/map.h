/***********************************************************
 *                                                         *
 * Copyright (c)                                           *
 *                                                         *
 * The Verifiable & Control-Theoretic Robotics (VECTR) Lab *
 * University of California, Los Angeles                   *
 *                                                         *
 * Authors: Kenny J. Chen, Ryan Nemiroff, Brett T. Lopez   *
 * Contact: {kennyjchen, ryguyn, btlopez}@ucla.edu         *
 *                                                         *
 ***********************************************************/

#include "dlio/dlio.h"

class dlio::MapNode {

public:

  MapNode(ros::NodeHandle node_handle);
  ~MapNode();

  void start();

  void callbackKeyframe(const sensor_msgs::PointCloud2ConstPtr& keyframe);

private:

  void getParams();

  bool savePcd(dlio_offline::save_pcd::Request& req,
               dlio_offline::save_pcd::Response& res);

  bool terminateMapNode(std_srvs::Empty::Request& req, 
                        std_srvs::Empty::Response& res);

  ros::NodeHandle nh;

  ros::Subscriber keyframe_sub;
  ros::Publisher map_pub;

  ros::ServiceServer save_pcd_srv;
  ros::ServiceServer terminate_srv;

  pcl::PointCloud<PointType>::Ptr dlio_map;

  std::string odom_frame;

  std::string map_filename;
};
