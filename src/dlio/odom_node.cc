/***********************************************************
 *                                                         *
 * Copyright (c)                                           *
 *                                                         *
 * The Verifiable & Control-Theoretic Robotics (VECTR) Lab *
 * University of California, Los Angeles                   *
 *                                                         *
 * Authors: Kenny J. Chen, Ryan Nemiroff, Brett T. Lopez   *
 * Contact: {kennyjchen, ryguyn, btlopez}@ucla.edu         *
 *                                                         *
 ***********************************************************/

#include "dlio/odom.h"
#include <rosbag/bag.h>
#include <rosbag/view.h>

int main(int argc, char** argv) {

  ros::init(argc, argv, "dlio_odom_node");
  ros::NodeHandle nh("~");

  dlio::OdomNode node(nh);
  node.start();

  std::string bag_filename;
  double offset_time;
  nh.getParam("bag_file", bag_filename);
  nh.getParam("bag_start_time", offset_time);

  rosbag::Bag bag;
  bag.open(bag_filename, rosbag::bagmode::Read);
  
  rosbag::View view_all(bag);
  ros::Time all_bag_start_time = view_all.getBeginTime();
  ros::Time bag_start_time = all_bag_start_time + ros::Duration(offset_time);

  rosbag::View view(bag, bag_start_time);
  
  // Diagnostic Information
  size_t total_message_count = view.size();
  size_t current_message = 0;
  ros::Time diagnostic_start_time = ros::Time::now();
  ros::Time previous_diagnostic_message_time = ros::Time::now();

  // Print new lines to prepare for 
  std::cout << " \n \n \n \n" << std::endl;

  for (const rosbag::MessageInstance msg : view) {
    if (msg.isType<sensor_msgs::PointCloud2>()) {
      sensor_msgs::PointCloud2ConstPtr msg_pc 
        = msg.instantiate<sensor_msgs::PointCloud2>();
      node.callbackPointCloud(msg_pc);

    } else if (msg.isType<sensor_msgs::Imu>()) {
      sensor_msgs::Imu::ConstPtr msg_imu
        = msg.instantiate<sensor_msgs::Imu>();
      node.callbackImu(msg_imu);
    }

    ros::spinOnce();

    // Diagnostics
    double progress_ratio = (double) current_message / total_message_count;
    ros::Duration elapsed_time = ros::Time::now() - diagnostic_start_time;
    ros::Duration remaining_time(elapsed_time.toSec() * (1/(progress_ratio + 0.00001) - 1));

    if ((ros::Time::now() - previous_diagnostic_message_time).toSec() > 0.5) {

      // Go back 5 lines
      std::cout << "\033[5F";

      std::cout << "+------------------------+" << std::endl;

      std::cout << "|" << std::left << std::setfill(' ') << std::setw(24)
        << "PROGRESS:       " 
          +  to_string_with_precision(progress_ratio*100, 2)
          + "%"
        << "|" << std::endl;

      std::cout << "|" << std::left << std::setfill(' ') << std::setw(24)
        << "TIME ELAPSED:   " 
          + formatDuration(elapsed_time)
        << "|" << std::endl;

      std::cout << "|" << std::left << std::setfill(' ') << std::setw(24)
        << "TIME REMAINING: " 
          + formatDuration(remaining_time)
        << "|" << std::endl;
        
      std::cout << "+------------------------+"
        << std::endl;
      previous_diagnostic_message_time = ros::Time::now();
    }

    current_message++;
  }

  // Save map
  ros::ServiceClient save_pcd_client = nh.serviceClient<dlio_offline::save_pcd>("/dlio_map/save_pcd");
  dlio_offline::save_pcd save_pcd_srv;

  if (!save_pcd_client.call(save_pcd_srv)) {
    ROS_ERROR("FAILED TO CALL SERVICE /dlio_map/save_pcd");
    return 1;
  }

  ros::ServiceClient terminate_mapnode_client = nh.serviceClient<std_srvs::Empty>("/dlio_map/terminate_map_node");
  std_srvs::Empty terminate_mapnode_srv;
  terminate_mapnode_client.call(terminate_mapnode_srv);

  return 0;

}
